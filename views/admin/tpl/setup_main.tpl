[{include file="headitem.tpl" title="GENERAL_ADMIN_TITLE"|oxmultilangassign}]
<script type="text/javascript">
<!--
function _groupExp(el) {
    var _cur = el.parentNode;

    if (_cur.className == "exp") _cur.className = "";
      else _cur.className = "exp";
}
//-->
</script>

[{ if $readonly }]
[{assign var="readonly" value="readonly disabled"}]
[{else}]
[{assign var="readonly" value=""}]
[{/if}]
<form name="transfer" id="transfer" action="[{ $oViewConf->getSelfLink() }]" method="post">
    [{ $oViewConf->getHiddenSid() }]    
    <input type="hidden" name="oxid" value="[{ $oxid }]">
    <input type="hidden" name="cl" value="setup_main">
    <input type="hidden" name="fnc" value="">
    <input type="hidden" name="actshop" value="[{$oViewConf->getActiveShopId()}]">
    <input type="hidden" name="updatenav" value="">
    <input type="hidden" name="editlanguage" value="[{ $editlanguage }]">
</form>
<form name="myedit" id="myedit" action="[{ $oViewConf->getSelfLink() }]" method="post">
    [{ $oViewConf->getHiddenSid() }]
    <input type="hidden" name="cl" value="setup_main">
    <input type="hidden" name="fnc" value="save">
    <input type="hidden" name="language" value="[{ $actlang }]">
    <div class="groupExp">
        <div>
            <a href="#" onclick="_groupExp(this);return false;" class="rc"><b>[{oxmultilang ident='oxidcms_setup'}]</b></a>
            <dl>
                <dd>
                    [{foreach from=$languages key=lang item=olang}]
                        <fieldset>
                            <legend>[{ $olang->name }]</legend>
                            <table>
                                <tr>
                                    <td valign="top" class="edittext">
                                        [{oxmultilang ident="OxidCMS_SOURCE"}]:
                                    </td>
                                    <td valign="top" class="edittext">
                                            <input type="text" name="editval[aOxidCMSCurlSource][[{ $lang }]]" value="[{$aOxidCMSCurlSource.$lang}]">
                                    </td>
                                </tr>
                                <tr>
                                    <td valign="top" class="edittext">
                                        [{oxmultilang ident="OxidCMS_SOURCE_SSL"}]: 
                                    </td>
                                    <td valign="top" class="edittext">
                                            <input type="text" name="editval[aOxidCMSCurlSourceSsl][[{ $lang }]]" value="[{$aOxidCMSCurlSourceSsl.$lang}]">
                                    </td>
                                </tr>
                                <tr>
                                    <td valign="top" class="edittext">
                                        [{oxmultilang ident="OxidCMS_SEARCH_URL"}]: 
                                    </td>
                                    <td valign="top" class="edittext">
                                            <input type="text" name="editval[aOxidCMSSearchUrl][[{ $lang }]]" value="[{$aOxidCMSSearchUrl.$lang}]">
                                    </td>
                                </tr>
                                <tr>
                                    <td valign="top" class="edittext">
                                        [{oxmultilang ident="OxidCMS_PARAM"}]: 
                                    </td>
                                    <td valign="top" class="edittext">
                                            <input type="text" name="editval[aOxidCMSCurlUrlParams][[{ $lang }]]" value="[{$aOxidCMSCurlUrlParams.$lang}]">
                                    </td>
                                </tr>
                                <tr>
                                    <td valign="top" class="edittext">
                                        [{oxmultilang ident="OxidCMS_SEO_SNIPPET"}]: 
                                    </td>
                                    <td valign="top" class="edittext">
                                            <input type="text" name="editval[aOxidCMSCurlSeoSnippets][[{ $lang }]]" value="[{$aOxidCMSCurlSeoSnippets.$lang}]">
                                    </td>
                                </tr>
                            </table>
                        </fieldset>
                    [{/foreach}]
                    <fieldset>
                        <legend>[{oxmultilang ident="OxidCMS_GENERAL"}]</legend>
                        <input type="checkbox" name="editval[OxidCMSDontRewriteUrls]" value="1" [{if $OxidCMSDontRewriteUrls}]checked="checked"[{/if}]>
                        [{oxmultilang ident="OxidCMS_DONT_REWRITE"}]
                    </fieldset>
                </dd>
            </dl>
        </div>
    </div>
    <br />
    <input type="submit" class="edittext" id="oLockButton" value="[{ oxmultilang ident="GENERAL_SAVE" }]" onclick="Javascript:document.myedit.fnc.value='save'"" [{ $readonly }]>
    <br>

</form>

[{include file="bottomnaviitem.tpl"}]
[{include file="bottomitem.tpl"}]