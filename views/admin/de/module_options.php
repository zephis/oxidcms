<?php
$sLangName  = "Deutsch";
$aLang = array(
    'charset'                                   => 'utf-8',
    'SHOP_MODULE_GROUP_oxidcms_config_not_here'   => 'Konfiguration unter Erweiterungen > Oxid CMS Einstellungen',
    'SHOP_MODULE_noConfigHere'                  => 'Die Konfiguration ist nicht hier zu finden!',
);