<?php

namespace Zephis\CMS\Controller\Admin;

use OxidEsales\Eshop\Application\Controller\Admin\AdminController;
use OxidEsales\Eshop\Core\Registry;
use OxidEsales\Eshop\Core\Request;


class SetupMain extends AdminController
{
    protected $_sThisTemplate = 'setup_main.tpl';

    const CONFIG_MODULE_NAME = 'module:oxidcms';

    public function render()
    {
        $oConf = Registry::getConfig();
        $this->_aViewData['aOxidCMSCurlSource']       = $oConf->getShopConfVar('aOxidCMSCurlSource');
        $this->_aViewData['aOxidCMSCurlSourceSsl']    = $oConf->getShopConfVar('aOxidCMSCurlSourceSsl');
        $this->_aViewData['aOxidCMSSearchUrl']        = $oConf->getShopConfVar('aOxidCMSSearchUrl');
        $this->_aViewData['aOxidCMSCurlUrlParams']    = $oConf->getShopConfVar('aOxidCMSCurlUrlParams');
        $this->_aViewData['aOxidCMSCurlSeoSnippets']  = $oConf->getShopConfVar('aOxidCMSCurlSeoSnippets');
        $this->_aViewData['OxidCMSDontRewriteUrls']   = $oConf->getShopConfVar('OxidCMSDontRewriteUrls');
        return parent::render();
    }

    /**
     * Saves the settings
     * @return void
     */
    public function save()
    {
        $oConf = Registry::getConfig();
        $aParams = (new Request())->getRequestEscapedParameter('editval');;
        if($aParams['OxidCMSDontRewriteUrls'] != '1')
        {
            $aParams['OxidCMSDontRewriteUrls'] = 0;
        }
        $sShopId = $oConf->getShopId();
        $oConf->saveShopConfVar( 'arr', 'aOxidCMSCurlSource', $aParams['aOxidCMSCurlSource'], $sShopId, self::CONFIG_MODULE_NAME );
        $oConf->saveShopConfVar( 'arr', 'aOxidCMSCurlSourceSsl', $aParams['aOxidCMSCurlSourceSsl'], $sShopId, self::CONFIG_MODULE_NAME );
        $oConf->saveShopConfVar( 'arr', 'aOxidCMSSearchUrl', $aParams['aOxidCMSSearchUrl'], $sShopId, self::CONFIG_MODULE_NAME );
        $oConf->saveShopConfVar( 'arr', 'aOxidCMSCurlUrlParams', $aParams['aOxidCMSCurlUrlParams'], $sShopId, self::CONFIG_MODULE_NAME );
        $oConf->saveShopConfVar( 'arr', 'aOxidCMSCurlSeoSnippets', $aParams['aOxidCMSCurlSeoSnippets'], $sShopId, self::CONFIG_MODULE_NAME );
        $oConf->saveShopConfVar( 'bl', 'OxidCMSDontRewriteUrls', $aParams['OxidCMSDontRewriteUrls'], $sShopId, self::CONFIG_MODULE_NAME );
    }
}