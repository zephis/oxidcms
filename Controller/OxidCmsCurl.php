<?php
namespace Zephis\CMS\Controller;

use OxidEsales\Eshop\Application\Controller\FrontendController;
use OxidEsales\Eshop\Core\Language;
use Zephis\CMS\Core\Curl;

class OxidCmsCurl extends FrontendController
{
    /**
     * Current view template
     * @var string
     */
    protected $_sThisTemplate = 'oxidcms_curl.tpl';
    
    /**
     * stores URL from which typo3 content is loaded.
     * @var array
     */
    protected $_aSourceUrlByLang = null;

    protected $_sUrl;
    
    /**
     * sets canonical URL to the default Blog URL
     *
     * if the blog is accessible through (T)OXID & Typo3, this should be a good way to prevent duplicate content
     * @var string
     */
    public function getCanonicalUrl()
    {
        if ($this->_aSourceUrlByLang === null) {
            $this->_aSourceUrlByLang = $this->getConfig()->getConfigParam('aOxidCMSCurlSource');
        }

        $this->_aSeoUrls = $this->getConfig()->getConfigParam('aOxidCMSSeoSnippets');
        
        if ($iLangId === null) {
            $iLangId = (new Language())->getBaseLanguage();
        }
        
        $this->_sUrl = $this->getConfig()->getConfigParam('aSeoUrls');
        $sRegUri = preg_replace("/^\/".$this->_sUrl."\//i","",$_SERVER["REQUEST_URI"]);

        return $this->_aSourceUrlByLang[$iLangId].$sRegUri;
    }
    
    /**
     * Template variable getter. Returns tag title
     *
     * @return string
     */
    public function getTitle()
    {
        $sTitle = Curl::getInstance()->_sPageTitle;
        return $sTitle;
    }
    
    /**
     * Template variable getter. Returns meta description
     *
     * @return string
     */
    public function getMetaDescription()
    {
        $this->_xMetaDescription = strip_tags(Curl::getInstance()->_sPageDescription);
        if ( $this->_sMetaDescription === null ) {
            $this->_sMetaDescription = false;

            // set special meta description ?
            if ( ( $sDescription = $this->_xMetaDescription ) ) {
                $this->_sMetaDescription = $sDescription;
            } else {
                $this->_sMetaDescription = $this->_prepareMetaDescription( $this->_xMetaDescription );
            }
        }
        
        return $this->_sMetaDescription;
    }
    
    /**
     * Template variable getter. Returns meta keywords
     *
     * @return string
     */
    public function getMetaKeywords()
    {
        $this->_xMetaKeywords = strip_tags(Curl::getInstance()->_sPageKeywords);
        if ( $this->_sMetaKeywords === null ) {
            $this->_sMetaKeywords = false;

            // set special meta keywords ?
            if ( ( $sKeywords = $this->_xMetaKeywords ) ) {
                $this->_sMetaKeywords = $sKeywords;
            } else {
                $this->_sMetaKeywords = $this->_prepareMetaKeyword( $this->_xMetaKeywords, true );
            }
        }

        return $this->_sMetaKeywords;
    }
    
    /**
     * regular render function
     */
    public function render()
    {
        return parent::render();
    }
}
