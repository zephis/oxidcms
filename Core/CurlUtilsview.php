<?php
namespace Zephis\CMS\Core;

use OxidEsales\EshopCommunity\Core\Registry;

class CurlUtilsview extends \OxidEsales\Eshop\Core\UtilsView
{
    public function _fillCommonSmartyProperties($oSmarty)
    {
        parent::_fillCommonSmartyProperties($oSmarty);

        $cfg = Registry::getConfig();

        $aPluginsDir = $oSmarty->plugins_dir;
        $aPluginsDir[] = $cfg->getModulesDir()."/zephis/oxidcms/smarty/plugins/";

        $oSmarty->plugins_dir = $aPluginsDir;
    }
}