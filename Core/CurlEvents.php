<?php
namespace Zephis\CMS\Core;

use OxidEsales\Eshop\Core\Registry;

class CurlEvents extends \OxidEsales\Eshop\Core\Model\MultiLanguageModel {

	public static function onActivate() {

		$cfg = Registry::getConfig();

		//clearing cache
		$dir = $cfg->getConfigParam("sCompileDir")."*";
		foreach (glob($dir) as $item) {
			if (!is_dir($item)) {
				unlink($item);
			}
		}

		// reloading smarty object after activation
        Registry::get("oxUtilsView")->getSmarty(true);
	}

	public static function onDeactivate() {
		// reloading smarty object after deactivationg
		// but blocks are still in tempaltes -> exception
		// needs some optimization / workaround here, cause custom plugins dir is still in smarty object

		//oxRegistry::get("oxUtilsView")->getSmarty(true);


		//clearing cache to force re-init smarty object (i hope)
		$cfg = Registry::getConfig();
		$dir = $cfg->getConfigParam("sCompileDir")."*";
		foreach (glob($dir) as $item) {
			if (!is_dir($item)) {
				unlink($item);
			}
		}
	}

}

