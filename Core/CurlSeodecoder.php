<?php
namespace Zephis\CMS\Core;

use OxidEsales\Eshop\Core\Language;
use OxidEsales\Eshop\Core\Registry;

class CurlSeodecoder extends \OxidEsales\Eshop\Core\SeoDecoder
{
    
    public function decodeUrl( $sSeoUrl )
    {
        // check, if SeoUrl starts with t3contenturl
        $blIsToxidPage = $this->detectToxidAndLang($sSeoUrl);
        if( !$blIsToxidPage )
        {
            return parent::decodeUrl($sSeoUrl);
        }else{
            $aRet['cl'] = 'oxidcms_curl';
            $aRet['lang'] = $blIsToxidPage['lang'];
            $lang = new Language();
            $lang->setBaseLanguage($aRet['lang']);
            $toxidUrl =  $blIsToxidPage['url'];
            Registry::getConfig()->setConfigParam('sOxidCMSCurlPage',$toxidUrl);
            return $aRet;
        }
    }

    protected function detectToxidAndLang($sSeoUrl){
        $seoSnippets = Registry::getConfig()->getConfigParam('aOxidCMSCurlSeoSnippets');
        foreach($seoSnippets as $langId => $snippet)
        {
            if(strpos( $sSeoUrl, $snippet.'/') !== FALSE)
            {
                $aUrlSplit = explode($snippet.'/', $sSeoUrl);
                $toxidInfo = array(
                                'lang' => $langId,
                                'url' => $aUrlSplit[1],
                             );
                return $toxidInfo;
            }
        }
        return false;
    }
}
