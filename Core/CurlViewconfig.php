<?php
namespace Zephis\CMS\Core;

use OxidEsales\Eshop\Core\Registry;

class CurlViewconfig extends \OxidEsales\Eshop\Core\ViewConfig
{
    /**
     * calls _injectTplVariable and returns parent constructor result
     */
    public function __construct()
    {
        $this->_injectTplVariable();
        return parent::__construct();
    }

    /**
     * if config variable sTplVariable is set,
     * sets template variable with getToxid() result
     * @return void
     */
    protected function _injectTplVariable()
    {
        $oConfig = Registry::getConfig();
        $sTplVariableName = $oConfig->getConfigParam('sTplVariable');
        if ($sTplVariableName) {
            $oConfig->getActiveView()->addTplParam($sTplVariableName, $this->getToxid());
        }
    }

    /**
     * returns instance of toxidCurl
     * @return Curl
     */
    public function getToxid()
    {
        return Curl::getInstance();
    }
}