<?php
/**
 * This file is part of OXID eSales WYSIWYG module.
 *
 * OXID eSales WYSIWYG module is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OXID eSales WYSIWYG module is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OXID eSales WYSIWYG module.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2017
 * @version   OXID eSales WYSIWYG
 */

/**
 * Metadata version
 */
$sMetadataVersion = '2.0';

/**
 * Module information
 */
$aModule = array(
    'id'          => 'oxidcms',
    'title'       => 'Oxid CMS Bridge',
    'description' => array(
        'de' => '',
        'en' => '',
    ),
    'thumbnail'   => 'logo.png',
    'version'     => '0.0.1',
    'author'      => 'Zephis GmbH',
    'url'         => 'https://www.zephis.de',
    'email'       => 'info@zephis.de',
    'extend'      => array(
        \OxidEsales\Eshop\Core\SeoDecoder::class => \Zephis\CMS\Core\CurlSeodecoder::class,
        \OxidEsales\Eshop\Core\UtilsView::class => \Zephis\CMS\Core\CurlUtilsview::class,
        \OxidEsales\Eshop\Core\ViewConfig::class => \Zephis\CMS\Core\CurlViewconfig::class
    ),
    'controllers'       => array(
        'setup_main' => \Zephis\CMS\Controller\Admin\SetupMain::class,
        'oxidcms_curl' => \Zephis\CMS\Controller\OxidCmsCurl::class
    ),
    'templates'   => array(
        'setup_main.tpl'      => 'zephis/oxidcms/views/admin/tpl/setup_main.tpl',
    ),
    'events'      => array(
        'onActivate'    => '\Zephis\CMS\Core\CurlEvents::onActivate',
        'onDeactivate'  => '\Zephis\CMS\Core\CurlEvents::onDeactivate'
    ),
    'blocks'      => array(),
    'settings' => array(
        array(
            'group' => 'toxid_config_not_here',
            'name'  => 'noConfigHere',
        ),
    ),
);
